import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/pageStatic.vue'
import Intro from '../components/intro.vue'
import Blog from '../components/blogMain.vue'
import Post from '../components/fullPost.vue'
import Login from '../components/login.vue'
import Register from '../components/register.vue'


/* eslint-disable */ 
Vue.use(Router)

export const router = new Router({
  mode:'history',
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
        path: '/',
        name: 'intro',
        component: Intro
    },
    {
      path: '/blog',
      name: 'blog',
      component: Blog
    },
   {
     path: '/fullPost',
     name: 'fullPost',
     component: Post
   },
   {
     path: '/login',
     name: 'login',
     component: Login
   },
   {
     path: '/register',
     name: 'register',
     component: Register
   }

  ]
});


// router.beforeEach((to, from, next) => {
//   redirect to login page if not logged in and trying to access a restricted page
  
//   const publicPages = ['/home', '/about','/login'];
//   const authRequired = !publicPages.includes(to.path);
//   const loggedIn = localStorage.getItem('user');
//   const loggedInData = JSON.parse(loggedIn);
   
  
//   if (authRequired && !loggedIn) {
//     return next('/home');
//   }
 
//   //console.log("From Router.js:"+loggedInData.fieldOfPractice);
 
//   next();


// })

  // router.beforeEach((to,from,next)  =>{
  //   console.log("going to "+to.name)
  //   if(from.name=="login"&&to.name=="blog")
  //   {
  //     vm.$forceUpdate()
  //   }
  //   next();
  // })
