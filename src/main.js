import Vue from 'vue'
//import Vuex from 'vuex'
import './plugins/vuetify'
import App from './App.vue'
import VideoBg from 'vue-videobg';
import { router } from './__helpers'
import '@fortawesome/fontawesome-free/css/all.css'
import VueYouTubeEmbed from 'vue-youtube-embed'
Vue.use(VueYouTubeEmbed)
Vue.config.productionTip = false

Vue.component('video-bg', VideoBg)
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
